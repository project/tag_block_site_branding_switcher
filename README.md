# TAG BLOCK SITE BRANDING SWITCHER

Provide a way to switch HTML Tag from Frontpage to other pages.

## INTRODUCTION

The native block-system-branding-block specifies site_logo, site_name and 
site_slogan variables displayed in div HTML Tag.
This module add h1 tag in frontpage for branding-block and keep the native 
display for the others pages.

* For a full description of the module, visit the project page: 
  https://drupal.org/project/tag_block_site_branding_switcher
* To submit bug reports and feature suggestions, or to track changes:
  https://drupal.org/project/issues/tag_block_site_branding_switcher
  
## REQUIREMENTS

This module requires no modules outside of Drupal core.
 
## RECOMMENDED MODULES

Markdown filter (https://www.drupal.org/project/markdown):
When enabled, display of the project's README.md help will be rendered
with markdown.

## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit:
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
for further information.

## CONFIGURATION

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the links from appearing. To get the links
back, disable the module and clear caches.


## MAINTAINERS

This project has been sponsored by:

[Insign](https://www.drupal.org/insign)
